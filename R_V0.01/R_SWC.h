#ifndef R_SWC_H
#define R_SWC_H

#include "R_config.h"

#define MaxMapSize 1000
#define Kp 0.75  //1.0
#define Ki 0.1  //0.5
#define Kd 0.2  //0.2

typedef struct 
{
    int16_t psLeft;
    int16_t psRight;
}WheelPosition_t;

extern WheelPosition_t WheelPosition_Map[MaxMapSize];


void SWC_PowerManger();
void SWC_Map(SWC_MAP_STA_t *p_swc_map_sta);
void SWC_ReplayMap(SWC_MAP_STA_t *p_swc_map_sta);
float SWC_ReplayMap_PIDControl_L(int16_t target,int16_t current);
float SWC_ReplayMap_PIDControl_R(int16_t target,int16_t current);
void SWC_BLE_Act();
#endif
