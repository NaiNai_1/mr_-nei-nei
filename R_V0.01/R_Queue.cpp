#include "R_config.h"
#include "R_Queue.h"


void Queue_Init(R_Queue_t *QueueList)
{
    for (uint8_t i = 0; i < 20; i++)
    {
        free(QueueList[i].DataAddr);
        QueueList[i].DataAddr = NULL;
    }
}
void Queue_Push(R_Queue_t *QueueList,uint8_t *p_data,uint8_t datalen)
{
    static uint8_t push_index = 0;
    QueueList[push_index].DataAddr = (uint8_t *)malloc(datalen);
    
    memcpy(QueueList[push_index].DataAddr,p_data,datalen);
    QueueList[push_index].DataLen = datalen;  
    push_index++;
    if (push_index >= 20)
    {
        push_index = 0;
    }
    
}
bool Queue_Pop(R_Queue_t *QueueList,uint8_t *p_data,uint16_t *datalen)
{
    static uint8_t pop_index = 0;
    if(QueueList[pop_index].DataAddr == NULL)
    {
        pop_index++;
        if (pop_index >=20)
        {
            pop_index = 0;
        }
        return false;
    }
    else
    {
        *datalen = QueueList[pop_index].DataLen;
        memcpy(p_data,QueueList[pop_index].DataAddr,QueueList[pop_index].DataLen);    //copy data from queue to the buffer
        free(QueueList[pop_index].DataAddr);                     //free the memory of the queue
        QueueList[pop_index].DataAddr = NULL;
        pop_index++;                                    //increment the index of the queue
        if (pop_index >=20)
        {
            pop_index = 0;
        }
        return true;
    }

    
    
    
}

void ValueLimitation(void *p_data, int min, int max)
{
    // int p_regdata = (int)(*p_data);
    // if (p_regdata < min)
    // {
    //     p_regdata = min;
    // }
    // if (p_regdata > max)
    // {
    //     p_regdata = max;
    // }
    // p_data
}