#ifndef R_SWC_H
#define R_SWC_H

#include "R_config.h"

#define MaxMapSize 3000
#define Kp 0.75  //1.0
#define Ki 0.1  //0.5
#define Kd 0.2  //0.2
#define PID_StopFloat   15
#define Map_ReplayCount 50
typedef struct 
{
    int16_t psLeft;
    int16_t psRight;
}WheelPosition_t;

extern WheelPosition_t WheelPosition_Map[MaxMapSize];
extern bool Flag_100us;
extern bool Flag_200us;
extern bool Flag_500us;
extern bool Flag_1ms;
extern bool Flag_5ms;
extern bool Flag_10ms;
extern bool Flag_100ms;
extern bool Flag_500ms;

void SWC_PowerManger();
void SWC_Map();
void SWC_ReplayMap();
float SWC_ReplayMap_PIDControl_L(int16_t target,int16_t current);
float SWC_ReplayMap_PIDControl_R(int16_t target,int16_t current);
void SWC_BLE_Act();
void SWC_BLE_Updata();
void SWC_ScreenShowBatter();
#endif
