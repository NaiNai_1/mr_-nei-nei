#ifndef R_CONFIG_H
#define R_CONFIG_H

#include <Arduino.h>
#include "Wire.h"
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>


#include "R_Net_Block.h"
#include "R_MCAL.h"
#include "R_BSW.h"
#include "R_RTE.h"
#include "R_SWC.h"
#include "R_Queue.h"
/// @brief GPIO引脚定义

#define PIN_POWERVALUE 36
#define PIN_powerkey 34 
#define PIN_key1     35
#define PIN_key2     39
#define PIN_powerEN  5
#define PIN_ledred   2 		
#define PIN_ledblue  4 		

#define PIN_M_L      13
#define PIN_M_R 		 14


/// @brief LEDC通道编号
#define CH_RED      1
#define CH_BLUE     2

/// @brief I2C从机地址定义
typedef enum{
  LINESCAN_ADDR = 0x01,
  MOTODTIVER_ADDR,
}Salve_addr_t;
/// @brief 定义任务列表


/// @brief 类定义
class RobotMaster
{
  /// @brief 内部成员
private:
    
public:
    /// @brief setup函数
    void begin(); 
    void PowerManger(/* args */);

    /// @brief System任务函数
    void LedTask();
    void KeyTask();
    void I2cBusPopTask();
    void I2cBusPushTask();

    /// @brief 应用功能
    void Run_task();
    void Map_task();   // Map task function
    void Led_task();   

    void Check_Whole_task();
    void Replay_Map_task();
    void Follow_black_line_task();
};

typedef struct 
{
     bool task_1ms    = false;
     bool task_10ms   = false;
     bool task_100ms  = false;
     bool task_1000ms = false;
}R_TIME_t;

typedef struct
{
   
  bool powerkey_press = false;
  bool key1_press    = false;
  bool key2_press    = false;
  bool powerkey_release = false;
  bool key1_release   = false;
  bool key2_release   = false;

}System_Msg_t;

/// @brief 地图内每一个节点的信息：方向、走过的角度
typedef struct 
{
  uint8_t Side;  // 0: left, 1: right,
  uint16_t Angle; 
}Map_One_Node_t;

#define POWER_FULL 947
#define POWER_LOW  674

#define EN_WHEEL_LOG            0
#define EN_WHEEL_DIRECT_DRIVER        1
#define EN_WHEEL_DIRECT_DRIVER_Core0  0
#define EN_SCREEN_POP_LOG       0
#define EN_BLE_LOG              0
#define EN_RTE_LOG              0
#define EN_I2C_LOG              0
#define EN_TIME_LOG             0

extern xQueueHandle xQueue_Map;
extern R_Queue_t I2C_PopList[MAX_LEN_OF_QUEUE];           /// @brief I2C总线输出队列
extern R_Queue_t I2C_PushList[MAX_LEN_OF_QUEUE];          /// @brief I2C总线输入队    
extern R_Queue_t Map_List[2];

#endif