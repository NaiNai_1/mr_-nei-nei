#ifndef R_NET_BLOCK_H
#define R_NET_BLOCK_H

#include <WiFi.h>

//**************宏、全局常量********************
#define wifi_name  	"Samsung_Note10U"     		//WIFI名称
#define wifi_password "pyjiaoyi"  	//WIFI密码
 
#define server_ip 	"bemfa.com" 	//巴法云服务器地址
#define server_port "8344" 			//tcp创客云端口8344
 

 
#define MAX_PACKETSIZE 512			//最大字节数
#define KEEPALIVEATIME 60*1000		//设置心跳60s


// //******************全局变量**********************
extern String UID;  //用户私钥，可在控制台获取
extern String TOPIC;         	//主题名字，可在控制台新建
 
extern const int LED_Pin;             	//GPIO2，控LED的引脚
//tcp客户端相关初始化，默认即可
extern WiFiClient TCPclient;
extern String TcpClient_Buff;//初始化字符串，用于接收服务器发来的数据
extern unsigned int TcpClient_BuffIndex;
extern unsigned long TcpClient_preTick;
extern unsigned long preHeartTick;		//心跳
extern unsigned long preTCPStartTick;	//连接
extern bool preTCPConnected;


//******************函数声明**********************

class R_Net_Block
{
private:
   
public:
    //连接WIFI
    void WiFi_Config(void);		//配置WiFi的SSID和密码并连接（使能自动重连）
    
    //TCP初始化连接
    void sendtoTCPServer(String p);		//发送数据到TCP服务器
    void startTCPClient();				//初始化和服务器建立连接
    void doTCPClientTick();				//检查数据，发送心跳
};

#endif

