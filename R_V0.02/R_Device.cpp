#include "R_config.h"

System_Msg_t 	System_Msg;	

/**************************************************************
 *              初始化部分
 **************************************************************/
/** @brief 启动管理*/
void RobotMaster::begin()
{
    Wire.setPins(23,18);
    Wire.begin();

    pinMode(PIN_powerkey, INPUT);
    pinMode(PIN_key1, INPUT);
    pinMode(PIN_key2, INPUT);
    pinMode(PIN_powerEN, OUTPUT);

    ledcAttachPin(PIN_ledred,1);
    ledcAttachPin(PIN_ledblue,2);
  
    ledcSetup(CH_RED, 12000, 8); // 500hz freq, 8 bit resolution, ie. 1ms on, 1ms off per
    ledcSetup(CH_BLUE, 12000, 8); 
    
   /// @brief 
}

/** @brief 电源管理*/
void RobotMaster::PowerManger(/* args */)
{
  int i;
  /**
   * 电源键启动
  */
  if (analogRead(PIN_powerkey) >= 3500)
  {
    digitalWrite(PIN_powerEN, HIGH);
    /// @brief 松手才开始执行
    while(analogRead(PIN_powerkey) >= 3500);   
  } 
  else 
  {
    digitalWrite(PIN_powerEN, LOW); 
  }
  
  
} 
/**************************************************************
 *              任务部分
 **************************************************************/
void RobotMaster::LedTask()
{
    static uint8_t i;
    i++;
    if (i > 250)
    {
        i = 0;
    }
    ledcWrite(CH_RED,i);
    ledcWrite(CH_BLUE,i);
}

void RobotMaster::KeyTask()
{
  int i;
  uint16_t error_count =  0;
  if (analogRead(PIN_powerkey) >= 3500)
  {
    if (digitalRead(PIN_powerEN))
    {
      digitalWrite(PIN_powerEN, LOW);
    }
    else
    {
      digitalWrite(PIN_powerEN, HIGH);
    }
    while(analogRead(PIN_powerkey) >= 3500);
  }··
  if (digitalRead(PIN_key1))
  {
    System_Msg.key1_press = true;
    System_Msg.key1_release = false;
  }
  else
  {
    System_Msg.key1_press = false;
    System_Msg.key1_release = true;
  }
  if (digitalRead(PIN_key2))
  {
    System_Msg.key2_press = true;
    System_Msg.key2_release = false;
  }
  else
  {
    System_Msg.key2_press = false;
    System_Msg.key2_release = true;
  }         
  
}

void RobotMaster::I2cBusPopTask()
{
  Salve_addr_t s_addr;
  
  uint8_t reg_data[25];
  uint8_t i = 0;
  uint8_t reg_i2cdata[40];
  uint8_t bytesReceived;
  static uint8_t Pop_Index = 0;
  switch (Pop_Index)
  {
  case 0:
    s_addr = LINESCAN_ADDR;
    Wire.beginTransmission(s_addr);
    reg_data[0] = 0x10;
    Wire.write(reg_data,1);

    Wire.endTransmission(false);
    Wire.requestFrom(s_addr, 8);

    if((bool)bytesReceived){ //If received more than zero bytes
      Wire.readBytes(reg_i2cdata, bytesReceived);
      R_Queue.queuePush(I2C_PopList,reg_i2cdata,bytesReceived);
    }
    break;
  case 1:
    s_addr = MOTODTIVER_ADDR;
    Wire.beginTransmission(s_addr);
    reg_data[0] = 0x10;
    Wire.write(reg_data,1);

    Wire.endTransmission(false);
    Wire.requestFrom(s_addr, 15);

    if((bool)bytesReceived){ //If received more than zero bytes
      Wire.readBytes(reg_i2cdata, bytesReceived);
      R_Queue.queuePush(I2C_PopList,reg_i2cdata,bytesReceived);
    }
    break;
  
  default:
    Pop_Index = 0xFF;
    break;
  }
  Pop_Index++;  
}

void RobotMaster::I2cBusPushTask()
{

  uint8_t Push_Data[50];
  uint16_t Push_DataLen;
  uint8_t I2C_CRC;
  /// @brief 查询列表地址是否为空
  if (R_Queue.queuePop(I2C_PushList,Push_Data,&Push_DataLen))
  {
    Wire.beginTransmission(Push_Data[0]);
    Wire.write(Push_Data,Push_DataLen);
    Wire.endTransmission(false);
    Wire.requestFrom(Push_Data[0],1);
    Wire.readBytes(&I2C_CRC, 1);
  }
}