
#ifndef R_QUEUE_H
#define R_QUEUE_H

typedef struct 
{
  uint8_t *DataAddr;
  uint16_t DataLen;
}R_Queue_t;
/// @brief 全局变量
   

/// @brief 队列功能组
#define MAX_LEN_OF_QUEUE 20
void Queue_Init(uint8_t *QueueName);
void Queue_Push(R_Queue_t *QueueList,uint8_t *p_data,uint8_t datalen);
bool Queue_Pop(R_Queue_t *QueueList,uint8_t *p_data,uint16_t *datalen);
void ValueLimitation(void *p_data, int min, int max);
#endif