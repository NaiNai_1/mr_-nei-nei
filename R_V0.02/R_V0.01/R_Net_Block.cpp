#include "R_config.h"
 

 
 
//******************全局变量**********************

String UID = "6fb82e72ebd7428cfa4f74f1e36041fd";  //用户私钥，可在控制台获取
String TOPIC = "light002";         	//主题名字，可在控制台新建
 
const int LED_Pin = 2;             	//GPIO2，控LED的引脚

//tcp客户端相关初始化，默认即可
WiFiClient TCPclient;
String TcpClient_Buff = "";//初始化字符串，用于接收服务器发来的数据
unsigned int TcpClient_BuffIndex = 0;
unsigned long TcpClient_preTick = 0;
unsigned long preHeartTick = 0;		//心跳
unsigned long preTCPStartTick = 0;	//连接
bool preTCPConnected = false;


//******************函数实现**********************
//配置WiFi的SSID和密码并连接（使能自动重连）
void R_Net_Block::WiFi_Config(void)		
{
	WiFi.begin(wifi_name, wifi_password);    //设置需要连接的wifi的名称和密码
	Serial.print("\n正在连接wifi");
	while(WiFi.status()!= WL_CONNECTED)     //等待连接中...
	{
		delay(500);
		Serial.print(".");
	}
	Serial.print("\n已连接到wifi：");
	Serial.print(WiFi.SSID());              //输出WIFI的SSID
	Serial.print("\nIP地址：");
	Serial.print(WiFi.localIP());           //输出WiFi的IP地址信息	
	WiFi.setAutoReconnect(true);					//使能自动重连
}
 
 
//发送数据到TCP服务器
void R_Net_Block::sendtoTCPServer(String p){
	if (!TCPclient.connected()) 
	{
		Serial.println("Client is not readly");
		return;
	}
	TCPclient.print(p);
	preHeartTick = millis();			//更新心跳计时
}
 
//和服务器建立TCP连接，订阅主题
void R_Net_Block::startTCPClient(){
	if(TCPclient.connect(server_ip, atoi(server_port))){
		Serial.print("\nConnected to server:");
		Serial.printf("%s:%d\r\n",server_ip,atoi(server_port));
 
		String tcpTemp="";  //初始化字符串
		tcpTemp = "cmd=1&uid="+UID+"&topic="+TOPIC+"\r\n"; //构建订阅指令
		sendtoTCPServer(tcpTemp); //发送订阅指令
		tcpTemp="";//清空
		/*
		 //如果需要订阅多个主题，可发送  cmd=1&uid=xxxxxxxxxxxxxxxxxxxxxxx&topic=xxx1,xxx2,xxx3,xxx4\r\n
		教程：https://bbs.bemfa.com/64
		 */
 
		preTCPConnected = true;
		TCPclient.setNoDelay(true);
	}
	else{
		Serial.print("Failed connected to server:");
		Serial.println(server_ip);
		TCPclient.stop();
		preTCPConnected = false;
	}
	preTCPStartTick = millis();
}
 
 
//接收巴法云的数据并解析，定时发送心跳
void R_Net_Block::doTCPClientTick(){
	//检查WIFI是否断开，自动重连
	if(WiFi.status() != WL_CONNECTED) 
		return;
	//检测TCP是否连接
	if (!TCPclient.connected()) {		
		//TCP断开连接
		if(preTCPConnected == true){
			preTCPConnected = false;
			preTCPStartTick = millis();
			Serial.println();
			Serial.println("TCP Client disconnected.");
			TCPclient.stop();
		}
		else if(millis() - preTCPStartTick > 1*1000){//1s重新连接
			startTCPClient();
		}
	}
	else
	{	//TCP已连接
		if (TCPclient.available()) {	//收数据
			char c =TCPclient.read();
			TcpClient_Buff +=c;			//TcpClient_Buff存收到的数据
			TcpClient_BuffIndex++;
			TcpClient_preTick = millis();
 
			//超过MAX_PACKETSIZE，直接进行解析
			if(TcpClient_BuffIndex>=MAX_PACKETSIZE - 1){
				TcpClient_BuffIndex = MAX_PACKETSIZE-2;
				TcpClient_preTick = TcpClient_preTick - 200;
			}	 
		}
		//保持心跳
		if(millis() - preHeartTick >= KEEPALIVEATIME){
			preHeartTick = millis();
			Serial.println("Send Keep alive:");
			sendtoTCPServer("ping\r\n"); //发送心跳，指令需\r\n结尾，详见接入文档介绍
		}
	}
	//有数据，且距离上次收到数据已有200ms间隔（超过MAX_PACKETSIZE除外）
	if((TcpClient_Buff.length() >= 1) && (millis() - TcpClient_preTick>=200))
	{
		TCPclient.flush();
		Serial.print("Rev string: ");
		TcpClient_Buff.trim(); 			//去掉首位空格
		Serial.println(TcpClient_Buff); //打印接收到的消息
		String getTopic = "";
		String getMsg = "";
		if(TcpClient_Buff.length() > 15){//TcpClient_Buff是个字符串
			//此时会收到推送的指令，指令大概为 cmd=2&uid=xxx&topic=light002&msg=off
			//c语言字符串查找，查找&topic=位置，并移动7位
			int topicIndex = TcpClient_Buff.indexOf("&topic=")+7; 
			//c语言字符串查找，查找&msg=位置
			int msgIndex = TcpClient_Buff.indexOf("&msg=");
			//c语言字符串截取，截取到topic
			getTopic = TcpClient_Buff.substring(topicIndex,msgIndex);
			//c语言字符串截取，截取到消息
			getMsg = TcpClient_Buff.substring(msgIndex+5);
			Serial.print("topic:------");
			Serial.println(getTopic); //打印截取到的主题值
			Serial.print("msg:--------");
			Serial.println(getMsg);   //打印截取到的消息值
		}
		if(getMsg  == "on"){       //如果是消息==打开
			Serial.println("Turn ON");
			digitalWrite(LED_Pin,HIGH);
		}else if(getMsg == "off"){ //如果是消息==关闭
			Serial.println("Turn OFF");
			digitalWrite(LED_Pin,LOW);
		}
		TcpClient_Buff="";
		TcpClient_BuffIndex = 0;
	}
}
