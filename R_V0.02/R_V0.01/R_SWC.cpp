#include "R_config.h"

//调用的类声名


//// SWC层

///@brief 电源管理SWC
void SWC_PowerManger()
{
//   uint16_t reg_data = 0;  
//   reg_data = MCAL_Gpio_AnalogRead(PIN_powerkey);
//  if (Flag_500ms)
//  {
//     Flag_500ms = 0;
//     Serial.println(MCAL_Gpio_AnalogRead(PIN_POWERVALUE));  
//  }
 
  

  if (MCAL_Gpio_AnalogRead(PIN_powerkey) >= 3000)
  {
    MCAL_Gpio_Write(PIN_powerEN, 0);
    /// @brief 松手才开始执行
    while(MCAL_Gpio_AnalogRead(PIN_powerkey) >= 3000);   
    delay(500);
  } 
  else 
  {
    MCAL_Gpio_Write(PIN_powerEN, 1); 
  }
}

///@brief 线路记录SWC

void SWC_Map_RPort_LWheelF(uint8_t LWheel_F)
{
    Rte_Wheel_Front.LWheel = LWheel_F;
}
void SWC_Map_RPort_RWheelF(uint8_t RWheel_F)
{
    Rte_Wheel_Front.RWheel = RWheel_F;
}


static uint16_t Map_Index = 0;


uint16_t SWC_Map_ReadMapIndex()
{
    return Map_Index;
}
void SWC_Map_ResetMapIndex()
{
    Map_Index = 0 ;
    swc_map_sta = CREATE_MAP;
}
void SWC_Map()
{
    /** 
     * position
     * ↑
     * |
     * |
     * |
     * 丄---------->time
    */
    
    static WheelPosition_t WheelPosition_Map[MaxMapSize];                       /** 地图 */
    static SWC_MAP_STA_t swc_map_sta_ex = CREATE_MAP;                           
    static WheelPosition_t WheelPosition;
    static WheelPosition_t WheelPosition_ex;
    static uint8_t Wheel_Front;

    /***********************************************************************/
    /*                               地图绘制                               */
    /***********************************************************************/


    //地图输出
    if (swc_map_sta_ex != swc_map_sta && swc_map_sta == REPLAY_MAP)             /** 刚刚从WriteMap转入ReplayMap*/
    {
        swc_map_sta_ex = swc_map_sta;
        xQueueSendToFront(xQueue_Map, WheelPosition_Map, 0);
        return;
    }
    if(swc_map_sta == REPLAY_MAP)
    {
        swc_map_sta_ex = swc_map_sta;
        return;
    }
    
    swc_map_sta_ex = swc_map_sta;
    //写爆数组移位处理    
    if (Map_Index == MaxMapSize)
    {
        Map_Index--;
        int i;
        for (i = 0; i < MaxMapSize - 1; i++) {
            WheelPosition_Map[i].psLeft =  WheelPosition_Map[i + 1].psLeft;
            WheelPosition_Map[i].psRight =  WheelPosition_Map[i + 1].psRight;
        }
        
    }

    //坐标系起点设定
    if (Map_Index == 0)
    {
        // BSW_WheelRotation(&WheelPosition_Map[0].psLeft,&WheelPosition_Map[0].psRight);
        if(xQueueReceive(xQueue_WheelRotation,WheelPosition_Map,0) != pdPASS )
        {
            return;
        }
    }
    else if (Map_Index < MaxMapSize) 
    {
        WheelPosition_ex = WheelPosition;                                       /** 车轮输出值坐标系 深度    */
        // BSW_WheelRotation(&WheelPosition.psLeft,&WheelPosition.psRight);        /** 获取车轮位置            */
        if(xQueueReceive(xQueue_WheelRotation,&WheelPosition,0) != pdPASS)
        {
            return;
        }

        if (abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) < 2 &&
        abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) < 2)
        {
            return;
        }
        else if (abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) > 255 ||
        abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) > 255)
        {
            return;
        }

        //获取车轮位置 + 判断方向
       
        if (Rte_Wheel_Front.LWheel == W_F)              /** 左轮：车轮向前*/
        {

            WheelPosition_Map[Map_Index].psLeft     = WheelPosition_Map[Map_Index - 1].psLeft + abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;            
        }
        else if (Rte_Wheel_Front.LWheel == W_R)
        {
            WheelPosition_Map[Map_Index].psLeft     = WheelPosition_Map[Map_Index - 1].psLeft - abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;
        }
        else
        {
            WheelPosition_Map[Map_Index].psLeft = WheelPosition_Map[Map_Index - 1].psLeft;
        }

        if (Rte_Wheel_Front.RWheel == W_F)              /** 左轮：车轮向后*/
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight + abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
        }
        else if (Rte_Wheel_Front.RWheel == W_R)         /** 右轮：车轮向后*/
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight - abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
        }
        else
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight;
        }
        
        if (Map_Index == MaxMapSize -1)
        {
            MCAL_Gpio_Write(PIN_ledblue,1);
        }
        else
        {
            MCAL_Gpio_Write(PIN_ledblue,0);
        }
        // const TickType_t xTicksToWait = pdMS_TO_TICKS(100);

        Serial.print(Map_Index);
        Serial.print(",");
        Serial.print(WheelPosition_Map[Map_Index].psLeft);
        Serial.print(",");
        Serial.println(WheelPosition_Map[Map_Index].psRight);

        // Serial.print(abs(WheelPosition.psRight));
        // Serial.print("-");
        // Serial.print(abs(WheelPosition_ex.psRight));
        // Serial.print("=");
        // Serial.println((abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)));
    }   
    Map_Index++;
    
    /** 打印整个表 */
    // Serial.printf("Map:");
    // for(int table = 0;table < MaxMapSize; table++)
    // {
    //     Serial.printf("{%d,%d},",WheelPosition_Map[table].psLeft,WheelPosition_Map[table].psRight);
    //     if (table%10 == 0)
    //     {
    //         Serial.printf("\r\n");
    //     }
    // }
    // Serial.printf("\r\n");
}
void SWC_ReplayMap_Start()
{
    swc_map_sta = REPLAY_MAP;
}

void SWC_ReplayMap()
{
    static WheelPosition_t WheelPosition_Point;
    static uint16_t PointIndex = 0;                          /** 当前执行的标点*/
    static uint16_t StartPoint = 0;                          /** 当前执行的标点*/
    // static uint8_t PointIndex_R = 0;                          /** 当前执行的标点*/
    uint8_t WheelFront = 0;
    float Pid_out_L;
    float Pid_out_R;
    /** 物理量坐标系*/
    static WheelPosition_t WheelPosition;
    static WheelPosition_t WheelPosition_ex;
    /** 重播坐标系*/
    static WheelPosition_t Wheel_ReplayPoint;              /** 车轮物理位置_当前值 */ 
    static uint8_t ReplaySta = 0;                           /** 重播状态*/
    static WheelPosition_t WheelReplay_Map[MaxMapSize];
    static uint16_t ReplayLen = 0;                         /** 当前执行的标点*/

    /** SWC 测试 模拟 Map*/
    
    // char rev;
    // WheelPosition_t reg_map[] = 
    // {
    //    {-1100, 1100},{-990, 990},{-980, 980},{-970, 970},{-960, 960},{-950, 950},{-940, 940},{-930, 930},{-920, 920},{-910, 910},
    //    {-900, 900},{-890, 890},{-880, 880},{-870, 870},{-860, 860},{-850, 850},{-840, 840},{-830, 830},{-820, 820},{-810, 810},
    //    {-800, 800},{-790, 790},{-780, 780},{-770, 770},{-760, 760},{-750, 750},{-740, 740},{-730, 730},{-720, 720},{-710, 710},
    //    {-700, 700},{-690, 690},{-680, 680},{-670, 670},{-660, 660},{-650, 650},{-640, 640},{-630, 630},{-620, 620},{-610, 610},
    //    {-600, 600},{-590, 590},{-580, 580},{-570, 570},{-560, 560},{-550, 550},{-540, 540},{-530, 530},{-520, 520},{-510, 510},
    //    {-500, 500},{-490, 490},{-480, 480},{-470, 470},{-460, 460},{-450, 450},{-440, 440},{-430, 430},{-420, 420},{-410, 410},
    //    {-400, 400},{-390, 390},{-380, 380},{-370, 370},{-360, 360},{-350, 350},{-340, 340},{-330, 330},{-320, 320},{-310, 310},
    //    {-300, 300},{-290, 290},{-280, 280},{-270, 270},{-260, 260},{-250, 250},{-240, 240},{-230, 230},{-220, 220},{-210, 210},
    //    {-200, 200},{-190, 190},{-180, 180},{-170, 170},{-160, 160},{-150, 150},{-140, 140},{-130, 130},{-120, 120},{-110, 110},
    //    {-100, 100},{-90, 90},{-80, 80},{-70, 70},{-60, 60},{-50, 50},{-40, 40},{-30, 30},{-20, 20},{-10, 10},{0, 0}
    // };
   
   

    /** 更新重播点的位置*/
    
    if (ReplaySta == 0)
    {
        //if (Queue_Pop(Map_List,2,(uint8_t *)WheelReplay_Map,&ReplayLen))
        if(swc_map_sta == REPLAY_MAP)
        {
            if(uxQueueMessagesWaiting(xQueue_Map) != 0)
            {
                xQueueReceive(xQueue_Map,WheelReplay_Map,0);
                while (uxQueueMessagesWaiting(xQueue_Map) != 0)
                {
                    xQueueReceive(xQueue_Map,WheelReplay_Map,0);
                    delay(10);
                }
                
                /**开始回放*/
                StartPoint = SWC_Map_ReadMapIndex() - 1;
                PointIndex = StartPoint;
                Serial.printf("/****************************************************************/\r\n");
                Serial.printf("/*                       Start replay map                       */\r\n");
                Serial.printf("/****************************************************************/\r\n");
                ReplaySta = 1;
                Serial.printf("StartPoint:%d \r\n",StartPoint);
            }
        }

        
        // if(Serial.available())
        // {
        //     memcpy(WheelReplay_Map,reg_map,sizeof(WheelReplay_Map)); 
        //     StartPoint = 100;
        //     PointIndex = StartPoint;
        //     rev=Serial.read();
        //     Serial.printf("/**********************************************************************/\r\n");
        //     Serial.printf("/*                           Start by USRT                            */\r\n");
        //     Serial.printf("/**********************************************************************/\r\n");
        //     ReplaySta = 1;
        //     Serial.printf("StartPoint:%d \r\n",StartPoint);
        // }
    }

    

    
    
    
    if (ReplaySta == 1)
    {        
        
            
        if (swc_map_sta == CREATE_MAP)
        {
            ReplaySta = 0;
        }

        WheelPosition_ex = WheelPosition;
        BSW_WheelRotation(&WheelPosition.psLeft,&WheelPosition.psRight);
        // while(uxQueueMessagesWaiting(xQueue_WheelRotation))
        // {
        //     xQueueReceive(xQueue_WheelRotation,&WheelPosition,0);
        // }
        #if EN_I2C_LOG
        Serial.printf("I2C Queue Pop  -> WheelRotationMsg.psLeft: %d, WheelRotationMsg.psRight: %d, Time Count:%d\r\n",WheelPosition.psLeft,WheelPosition.psRight,Clock_Counter);
        #endif
        if (PointIndex == StartPoint)
        {
            /** 设置重播点的位置*/
            Wheel_ReplayPoint = WheelReplay_Map[PointIndex];
        }
        else
        {
             
            if (Rte_Wheel_Front.LWheel == W_F)    
            {

                Wheel_ReplayPoint.psLeft     = Wheel_ReplayPoint.psLeft + abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;            
            }
            else if (Rte_Wheel_Front.LWheel == W_R)
            {
                Wheel_ReplayPoint.psLeft     = Wheel_ReplayPoint.psLeft - abs(abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;
            }
            else
            {
                Wheel_ReplayPoint.psLeft = Wheel_ReplayPoint.psLeft;
            }

            if (Rte_Wheel_Front.RWheel == W_F)
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight + abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
            }
            else if (Rte_Wheel_Front.RWheel == W_R)
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight - abs(abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
            }
            else
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight;
            }
        }
       
        
        /** PID */
        Pid_out_L = SWC_ReplayMap_PIDControl_L(WheelReplay_Map[PointIndex].psLeft,Wheel_ReplayPoint.psLeft);
        Pid_out_R = SWC_ReplayMap_PIDControl_R(WheelReplay_Map[PointIndex].psRight,Wheel_ReplayPoint.psRight);
        Pid_out_L = Pid_out_L > 200 ? 200 : Pid_out_L;
        Pid_out_L = Pid_out_L < -200 ? -200 : Pid_out_L;
        Pid_out_R = Pid_out_R > 200 ? 200 : Pid_out_R;
        Pid_out_R = Pid_out_R < -200 ? -200 : Pid_out_R;
        /** 驱动电机 */
        #if EN_WHEEL_DIRECT_DRIVER
        BSW_WheelDriver(MOTO_L,POWER_DRIVE,Pid_out_L);
        BSW_WheelDriver(MOTO_R,POWER_DRIVE,Pid_out_R);
        #else
        WheelDriverMsg_t WheelDriverMsg;
        WheelDriverMsg.moto = MOTO_L;
        WheelDriverMsg.fun = POWER_DRIVE;
        WheelDriverMsg.data = Pid_out_L;
        xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
        WheelDriverMsg.moto = MOTO_R;
        WheelDriverMsg.fun  = POWER_DRIVE;
        WheelDriverMsg.data = Pid_out_R;
        xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
        #endif

        /** 重播目标值控制 */
        if (
            (WheelReplay_Map[PointIndex].psLeft - Map_ReplayCount < Wheel_ReplayPoint.psLeft && Wheel_ReplayPoint.psLeft < WheelReplay_Map[PointIndex].psLeft + Map_ReplayCount)
            &&
            (WheelReplay_Map[PointIndex].psRight - Map_ReplayCount < Wheel_ReplayPoint.psRight && Wheel_ReplayPoint.psRight < WheelReplay_Map[PointIndex].psRight + Map_ReplayCount)
            )
        {
            //向前重播
            // PointIndex++;
            // if (PointIndex == SWC_Map_ReadMapIndex())
            // { 
            //     PointIndex = SWC_Map_ReadMapIndex() - 1;
            // }
            
            //向后重播
            PointIndex  = PointIndex > 0 ? PointIndex - 1 : 0;
            if (
            (WheelReplay_Map[0].psLeft - PID_StopFloat < Wheel_ReplayPoint.psLeft && Wheel_ReplayPoint.psLeft < WheelReplay_Map[0].psLeft + PID_StopFloat)
            &&
            (WheelReplay_Map[0].psRight - PID_StopFloat < Wheel_ReplayPoint.psRight && Wheel_ReplayPoint.psRight < WheelReplay_Map[0].psRight + PID_StopFloat)
            )
            {
                Serial.printf("/**********************************************************************/\r\n");
                Serial.printf("/*                         Replay map finish                          */\r\n");
                Serial.printf("/**********************************************************************/\r\n");
                ReplaySta = 0;
                SWC_Map_ResetMapIndex();
                #if EN_WHEEL_DIRECT_DRIVER
                BSW_WheelDriver(MOTO_L,POWER_DRIVE,0);
                #else
                WheelDriverMsg.moto = MOTO_L;
                WheelDriverMsg.fun  = POWER_DRIVE;
                WheelDriverMsg.data = 0;
                xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
                #endif
                #if EN_WHEEL_DIRECT_DRIVER
                BSW_WheelDriver(MOTO_R,POWER_DRIVE,0);
                #else
                WheelDriverMsg.moto = MOTO_R;
                WheelDriverMsg.fun  = POWER_DRIVE;
                WheelDriverMsg.data = 0;
                xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
                #endif
                
                swc_map_sta = CREATE_MAP;
                
            }
        
        }
        // Serial.printf("Replay:%d,%d,%d\r\n",PointIndex,Wheel_ReplayPoint.psLeft,Wheel_ReplayPoint.psRight);
        // Serial.printf("ReplayOutPut:%f,%f\r\n",Pid_out_L,Pid_out_L);
        #if 0
        // if (PointIndex_R == MaxMapSize - 1 && PointIndex_L == MaxMapSize - 1)
        // {
        //     /** 回放结束 */
        //     (*p_swc_map_sta) = CREATE_MAP;
        //     swc_map_sta_ex = CREATE_MAP;
        // }
        #endif
    }
    
    
}

float SWC_ReplayMap_PIDControl_L(int16_t target,int16_t current)
{
    // PID控制器变量
    static float error = 0;
    static float integral = 0;
    static float derivative = 0;
    static float lastError = 0;
    static float Output;
    int16_t _float = 15; /** 进入第二层PID */
    //数值接近则清除积分值
    if (target - PID_StopFloat < current && current < target + PID_StopFloat)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    // if (abs(target - current) < 20 && Output > 150)
    // {
    //     error = 0;
    //     integral = 0;
    //     derivative = 0;
    //     lastError = 0;
    //     return 0;
    // }
    
    error = target - current;
    integral += error;
    derivative = error - lastError;
    lastError = error;
    // Serial.printf("PID:%f,%f,%f",Kp * error,Ki * integral,Kd * derivative);
    // Serial.printf("\r\n");
    Output = (Kp * error) + (Ki * integral) + (Kd * derivative);
    return Output;
}
float SWC_ReplayMap_PIDControl_R(int16_t target,int16_t current)
{
    // PID控制器变量
    static float error = 0;
    static float integral = 0;
    static float derivative = 0;
    static float lastError = 0;
    static float Output;
    int16_t _float = 15; /** 进入第二层PID */
    //数值接近则清除积分值
    if (target - PID_StopFloat < current && current < target + PID_StopFloat)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    // if (abs(target - current) < 20 && Output > 150)
    // {
    //     error = 0;
    //     integral = 0;
    //     derivative = 0;
    //     lastError = 0;
    //     return 0;
    // }
    
    error = target - current;
    integral += error;
    derivative = error - lastError;
    lastError = error;
    // Serial.printf("PID:%f,%f,%f",Kp * error,Ki * integral,Kd * derivative);
    // Serial.printf("\r\n");
    Output = (Kp * error) + (Ki * integral) + (Kd * derivative);
    return Output;
}
///@brief 遥控信号执行SWC BLE信号分发到 MOTO/LED
void SWC_BLE_Act()
{
    static uint8_t reg_data[20];
    static uint8_t ble_data_ex[20];
    uint16_t datalen;
    static int BleToWheel_R;
    static int BleToWheel_L;
    WheelDriverMsg_t WheelDriverMsg;
    /// @brief 蓝牙消息管理部分
    if(uxQueueMessagesWaiting(xQueue_BLE_Recv) != 0)
    {
        //BLE消息出队列
        memcpy(ble_data_ex,reg_data,sizeof(reg_data));
        xQueueReceive(xQueue_BLE_Recv,reg_data,0);

        #if EN_BLE_LOG
        Serial.printf("BLE_Recv:");
        for (uint8_t i = 0; i < 14; i++)
        {
            Serial.printf("%x ",reg_data[i]);
        }
        Serial.printf("\r\n");
        #endif
        

        while (uxQueueMessagesWaiting(xQueue_BLE_Recv) != 0)
        {
            memcpy(ble_data_ex,reg_data,sizeof(reg_data));
            xQueueReceive(xQueue_BLE_Recv,reg_data,0);

            #if EN_BLE_LOG
            Serial.printf("BLE_Recv:");
            for (uint8_t i = 0; i < 14; i++)
            {
                Serial.printf("%x ",reg_data[i]);
            }
            Serial.printf("\r\n");
            #endif
        }

        //是否允许地图回放功能
        if(reg_data[8] != ble_data_ex[8])
        {   
            //停止电机
            #if EN_WHEEL_DIRECT_DRIVER_Core0
            BSW_WheelDriver(MOTO_L,POWER_DRIVE,0);
            BSW_WheelDriver(MOTO_R,POWER_DRIVE,0);
            #else

            WheelDriverMsg.moto = MOTO_L;
            WheelDriverMsg.fun = POWER_DRIVE;
            WheelDriverMsg.data = 0;
            xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );

            WheelDriverMsg.moto = MOTO_R;
            WheelDriverMsg.fun  = POWER_DRIVE;
            WheelDriverMsg.data = 0;
            xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
            #endif
            if(reg_data[8]%2 == 1)
            {
                SWC_ReplayMap_Start();   
            }
            else
            {
                SWC_Map_ResetMapIndex();
            }
        }
        
        //驱动电机
        if (reg_data[8]%2 == 0)
        {
            if (BleToWheel_L != (int8_t)reg_data[5] * 40)
            {
                BleToWheel_L = (int8_t)reg_data[5] * 40;
                #if EN_WHEEL_DIRECT_DRIVER_Core0
                BSW_WheelDriver(MOTO_R,POWER_DRIVE,BleToWheel_R);
                #else
                
                WheelDriverMsg.moto = MOTO_L,
                WheelDriverMsg.fun = POWER_DRIVE,
                WheelDriverMsg.data = BleToWheel_L,
                xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
                
                #endif
            }
            if (BleToWheel_R != (int8_t)reg_data[6] * 40)
            {
                BleToWheel_R = (int8_t)reg_data[6] * 40;
                #if EN_WHEEL_DIRECT_DRIVER_Core0
                BSW_WheelDriver(MOTO_L,POWER_DRIVE,BleToWheel_L);
                #else
                
                WheelDriverMsg.moto = MOTO_R,
                WheelDriverMsg.fun = POWER_DRIVE,
                WheelDriverMsg.data = BleToWheel_R,
                xQueueSend( xQueue_WheelDriver, &WheelDriverMsg, 0 );
                
                #endif

            }

        }
        
        

        
        
        

        //打印队列
      
        // Serial.printf("Queue =>");
        // for (int i = 0; i < datalen; i++)
        // {
        //   Serial.printf("%x ",reg_data[i]);
        // }
        // Serial.printf("    ");
        // Serial.printf("Wheel Data:");
        // Serial.printf("%d && %d",BleToWheel_L,BleToWheel_R);
        // Serial.printf("    ");
        // Serial.printf("swc_map_sta:");
        // Serial.printf(swc_map_sta == REPLAY_MAP?"REPLAY_MAP":"CREATE_MAP");
        // Serial.printf("\r\n");


    }
    
    /// @brief 蓝牙功能管理部分
    MCAL_BLEMsgManager();
    
}
void SWC_BLE_Updata()
{
    uint8_t reg_data[12];
    memset(reg_data,0x00,sizeof(reg_data));
    if (deviceConnected)
    {
        MCAL_BLESend(reg_data,sizeof(reg_data));
    }
}
void SWC_ScreenShowBatter()
{
    uint32_t pixcel = 0x01FFFFFF;
    float PowerValue = (float)(MCAL_Gpio_AnalogRead(PIN_POWERVALUE) - POWER_LOW)/(float)(POWER_FULL - POWER_LOW);
    if(PowerValue > 1)
    {
        PowerValue = 1;
    }
    else if(PowerValue < 0)
    {
        PowerValue = 0;
    }
    pixcel = pixcel >> (uint32_t)(25*(1.0000-PowerValue));
    // BSW_ScreenShowPixcel(pixcel);
    // Serial.printf("PowerValue :");
    // Serial.printf("%d   ",MCAL_Gpio_AnalogRead(PIN_POWERVALUE));
    // Serial.printf("PowerLevel :");
    // Serial.printf("%f   ",PowerValue);
    xQueueSend( xQueue_ScreenShowPixcel, &pixcel, 0 );
}
