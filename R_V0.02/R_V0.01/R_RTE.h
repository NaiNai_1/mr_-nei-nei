
#ifndef R_RTE_H
#define R_RTE_H

#include "R_config.h"

#define 	W_F			0
#define 	W_R			1
#define   W_NULL  2
typedef struct 
{
  uint8_t LWheel;
  uint8_t RWheel;
}RTE_WHEEL_FRONT_t;
extern RTE_WHEEL_FRONT_t Rte_Wheel_Front;

//SWC的路线回放功能执行状态
typedef enum
{
    CREATE_MAP,
    REPLAY_MAP
}SWC_MAP_STA_t;
typedef struct 
{
    MOTO_NUMBER moto;
    MOTO_FUNCTION fun;
    int data;
}WheelDriverMsg_t;

extern SWC_MAP_STA_t swc_map_sta;           /** 是否开始创建地图*/
extern xQueueHandle xQueue_Map;
extern xQueueHandle xQueue_WheelRotation;  /** 车轮位置队列*/
extern xQueueHandle xQueue_WheelDriver;    
extern xQueueHandle xQueue_ScreenShowPixcel;
extern xQueueHandle xQueue_BLE_Recv;       /** BLE消息队列*/

/** S/R Interface */
void SWC_Map_RPort_LWheelF(uint8_t LWheel_F);
void SWC_Map_RPort_RWheelF(uint8_t RWheel_F);
void RTE_I2C();                               /** RTC 层 I2C任务*/
#endif

