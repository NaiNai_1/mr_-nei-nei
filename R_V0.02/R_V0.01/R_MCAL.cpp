#include "R_config.h"
#include "Wire.h"
#include <ESP32Servo.h>
#include <Arduino.h>
//调用的类声明
#define ENABLE_Wheel_Front_Print 0

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;

Servo servo1;
Servo servo2;



/// @brief 蓝牙Callback
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      static uint8_t reg_data[20];
      uint8_t reg_data_ex[20];
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        #if EN_BLE_LOG
        Serial.printf("LE =>");
        #endif
        for (int i = 0; i < rxValue.length(); i++)
        {
            reg_data_ex[i] = reg_data[i];
                    #if EN_BLE_LOG
                    Serial.printf("%x ",rxValue[i]);
                    #endif
            reg_data[i] = rxValue[i];
        }
        #if EN_BLE_LOG
        Serial.printf("\r\n");
        #endif


        if (memcmp(reg_data_ex,reg_data,rxValue.length()) != 0)
        {
            xQueueSend(xQueue_BLE_Recv,reg_data,0);
            #if EN_BLE_LOG
            Serial.printf("LE Push End\r\n");

            #endif
        }
        
      }
    }
};

///@brief 蓝牙处理基础软件
void MCAL_BLEInit()
{
    // Create the BLE Device
  BLEDevice::init("TUDAO_ESP32");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
										CHARACTERISTIC_UUID_TX,
										BLECharacteristic::PROPERTY_NOTIFY
									);
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
											 CHARACTERISTIC_UUID_RX,
											BLECharacteristic::PROPERTY_WRITE
										);

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}
void MCAL_BLEMsgManager()
{
    int8_t reg_data = 0;
    // connected
    if (deviceConnected)
    {

    }
    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        MCAL_BLEAdvertising();
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
		// do stuff here on connecting
        oldDeviceConnected = deviceConnected;
        // delay(500);
        // MCAL_BLESend((uint8_t *)reg_data,1);
    }
}
void MCAL_BLESend(uint8_t *pData,uint32_t datalen)
{
    pTxCharacteristic->setValue(pData, datalen);
    pTxCharacteristic->notify();
}
void MCAL_BLEAdvertising()
{
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
}
void MCAL_BLEConneting()
{

}
///@brief 电机PWM驱动基础软件
void MCAL_FrequencyInit()
{
    servo1.setPeriodHertz(500);      // Standard 50hz servo
    servo2.setPeriodHertz(500);      // Standard 50hz servo
    servo1.attach(PIN_M_L);//13
    servo2.attach(PIN_M_R);//14
    servo1.writeMicroseconds(1500);
    servo2.writeMicroseconds(1500);
}
void MCAL_FrequencyPush(uint8_t channel,uint16_t Frequency)
{
    switch (channel)
    {
    case 0:
        //发送转速
        servo1.writeMicroseconds(Frequency);
        if (Frequency < 1500 - 15)
        {
            SWC_Map_RPort_LWheelF(0);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel Front");
            #endif
        }
        else if(Frequency > 1500 + 15)
        {
            SWC_Map_RPort_LWheelF(1);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel Back");
            #endif
        }
        else
        {
            SWC_Map_RPort_LWheelF(2);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel stop");
            #endif
        }

            #if ENABLE_Wheel_Front_Print
            Serial.printf("\r\n");
            #endif
        break;
    case 1:
        servo2.writeMicroseconds(Frequency);
        if (Frequency > 1500 + 15)
        {
            SWC_Map_RPort_RWheelF(0);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel Front");
            #endif
        }
        else if(Frequency < 1500 - 15)
        {
            SWC_Map_RPort_RWheelF(1);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel Back");
            #endif
        }
        else
        {
            SWC_Map_RPort_RWheelF(2);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel stop");
            #endif
        }

            #if ENABLE_Wheel_Front_Print
            Serial.printf("\r\n");
            #endif
        break;
    default:
        break;
    }
}
///@brief I2C驱动基础软件
void MCAL_I2cWriteStart(uint8_t addr,uint8_t *p_data,uint8_t len,bool StopFlag)
{
    uint8_t error;
    Wire.beginTransmission(addr);
    Wire.write(p_data,len);
    error = Wire.endTransmission(StopFlag);
    /*
    0: success.
    1: data too long to fit in transmit buffer.
    2: received NACK on transmit of address.
    3: received NACK on transmit of data.
    4: other error.
    5: timeout
    */
   switch (error)
   {
   case 1:
        Serial.printf("/************IIC Write error:  data too long to fit in transmit buffer***************/\r\n");
    break;
   case 2:
        Serial.printf("/************IIC Write error:  received NACK on transmit of address***************/\r\n");
    break;
   case 3:
        Serial.printf("/************IIC Write error:  received NACK on transmit of data***************/\r\n");
    break;
   case 4:
        Serial.printf("/************IIC Write error:  other error***************/\r\n");
    break;
   case 5:
        Serial.printf("/************IIC Write error:  timeout***************/\r\n");
    break;
   default:
    break;
   }
   
}
void MCAL_I2cReadStart(uint8_t addr,uint8_t *p_data,uint32_t len,bool StopFlag)
{
    uint8_t error;
    Wire.requestFrom(addr, len);
    Wire.readBytes(p_data, len);
    error = Wire.endTransmission(StopFlag);
    switch (error)
   {
   case 1:
        Serial.printf("/************IIC Read error:  data too long to fit in transmit buffer***************/\r\n");
    break;
   case 2:
        Serial.printf("/************IIC Read error:  received NACK on transmit of address***************/\r\n");
    break;
   case 3:
        Serial.printf("/************IIC Read error:  received NACK on transmit of data***************/\r\n");
    break;
   case 4:
        Serial.printf("/************IIC Read error:  other error***************/\r\n");
    break;
   case 5:
        Serial.printf("/************IIC Read error:  timeout***************/\r\n");
    break;
   default:
    break;
   }
}

uint16_t Clock_Counter = 0;
///@brief 定时器基础软件
#define _1MS_CLOCK 
TimerHandle_t timer;                              /// @brief 声明一个定时器
void timer_callback(TimerHandle_t xTimer) {       // 中断函数
  #ifdef _100US_CLOCK
  static uint16_t Clock_Counter = 0;
  Clock_Counter++;
  Flag_100us = 1;
  Flag_200us = Clock_Counter % 2 == 0 ?  1 : Flag_200us;
  Flag_500us = Clock_Counter % 5 == 0 ?  1 : Flag_500us;
  Flag_1ms = Clock_Counter % 10 == 0 ?  1 : Flag_1ms;
  Flag_5ms = Clock_Counter % 50 == 0 ?  1 : Flag_5ms;         /** 5ms*/
  Flag_10ms = Clock_Counter % 100 == 0 ?  1 : Flag_10ms;      /** 10ms*/
  Flag_100ms = Clock_Counter % 1000 == 0 ?  1 : Flag_100ms;   /** 100ms*/
  Flag_500ms = Clock_Counter % 5000 == 0 ?  1 : Flag_500ms;   /** 500ms*/
  Clock_Counter = Clock_Counter >= 5000 ? 0 : Clock_Counter;
  #endif 
  #ifdef    _1MS_CLOCK 
  Clock_Counter++;
  Flag_1ms = 1;
  Flag_5ms = Clock_Counter % 2 == 0 ?  1 : Flag_5ms;         /** 5ms*/
  Flag_10ms = Clock_Counter % 10 == 0 ?  1 : Flag_10ms;      /** 10ms*/
  Flag_100ms = Clock_Counter % 100 == 0 ?  1 : Flag_100ms;   /** 100ms*/
  Flag_500ms = Clock_Counter % 500 == 0 ?  1 : Flag_500ms;   /** 500ms*/
  Clock_Counter = Clock_Counter >= 500 ? 0 : Clock_Counter;
  #endif
  #ifdef    _100MS_CLOCK 
  Clock_Counter++;
  Flag_1ms = 1;
  Flag_5ms = 1;                                                 /** 5ms*/
  Flag_10ms = 1;                                                /** 10ms*/
  Flag_100ms = 1;                                               /** 100ms*/
  Flag_500ms = Clock_Counter % 5 == 0 ?  1 : Flag_500ms;      /** 500ms*/
  Clock_Counter = Clock_Counter >= 5 ? 0 : Clock_Counter;
  #endif

}

void MCAL_Timer_Init(uint32_t delay_data)
{
    timer = xTimerCreate(
    "MyTimer",                          // 定时器名称
    delay_data,                                  // 周期 ms
    pdTRUE,                             // 自动重置
    0,                                  // 定时器ID（未使用）
    timer_callback                      // 回调函数
  );
    xTimerStart(timer, 0);
    

}

///@brief MCU引脚驱动
void MCAL_Gpio_Write(uint8_t gpio_number,uint8_t value)
{
    if (value)
    {
        digitalWrite(gpio_number,HIGH);
    }
    else
    {
        digitalWrite(gpio_number,LOW);
    }
}
uint8_t MCAL_Gpio_Read(uint8_t gpio_number)
{
    return digitalRead(gpio_number);
}
uint16_t MCAL_Gpio_AnalogRead(uint8_t gpio_number)
{
    return analogRead(gpio_number);
}
void MCAL_PowerEnable(bool EnableFlag)
{
    if (EnableFlag)
    {
        /* code */
    }
}

void MCAL_Init()
{
    Wire.setPins(23,18);
    Wire.begin();

    pinMode(PIN_powerkey, INPUT);
    pinMode(PIN_key1, INPUT);
    pinMode(PIN_key2, INPUT);
    pinMode(PIN_powerEN, OUTPUT);

    pinMode(PIN_ledred, OUTPUT);
    pinMode(PIN_ledblue, OUTPUT);
    digitalWrite(PIN_powerEN, HIGH);
    // ledcAttachPin(PIN_ledred,1);
    // ledcAttachPin(PIN_ledblue,2);
  
    // ledcSetup(CH_RED, 12000, 8); // 500hz freq, 8 bit resolution, ie. 1ms on, 1ms off per
    // ledcSetup(CH_BLUE, 12000, 8); 
}
void MCAL_PowerKeep()
{
    int i;
  /**
   * 电源键启动
  */

  if (analogRead(PIN_powerkey) >= 3000)
  {
    digitalWrite(PIN_powerEN, HIGH);
    /// @brief 松手才开始执行
    while(analogRead(PIN_powerkey) >= 3000);   
  } 
  else 
  {
    digitalWrite(PIN_powerEN, LOW); 
  }

}


