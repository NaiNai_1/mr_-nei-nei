
#include "R_config.h"
#include "soc/rtc_wdt.h" //设置看门狗用
/// @brief  多任务初始化

TaskHandle_t xTask1;
TaskHandle_t xTask2;

/// @brief  队列初始化
R_Queue_t I2C_PopList[MAX_LEN_OF_QUEUE];  /// @brief I2C总线输出队列
R_Queue_t I2C_PushList[MAX_LEN_OF_QUEUE]; /// @brief I2C总线输入队

/// @brief 定时器初始化

void setup()
{

  Serial.begin(115200);
  MCAL_Init();
  MCAL_PowerKeep();
  MCAL_FrequencyInit();
  MCAL_BLEInit();
  MCAL_Timer_Init(1);

  // SWC Value Init
  swc_map_sta = CREATE_MAP;

  Wire.setTimeOut(1);

  Serial.print("Now Core Frequency:");
  Serial.println(getCpuFrequencyMhz());

  // Core Control
  xQueue_Map = xQueueCreate(3, sizeof(WheelPosition_t) * MaxMapSize); /** 地图数据队列*/
  xQueue_WheelRotation = xQueueCreate(8, sizeof(WheelPosition_t));    /** 车轮位置队列*/
  xQueue_WheelDriver = xQueueCreate(8, sizeof(WheelDriverMsg_t));     /** 车轮驱动队列*/
  xQueue_ScreenShowPixcel = xQueueCreate(8, sizeof(uint32_t));        /** 屏幕驱动队列*/
  xQueue_BLE_Recv = xQueueCreate(8, 20);                              /** 蓝牙数据队列*/
  xTaskCreatePinnedToCore(
      Core0Task,
      "Core0Task", /* 任务名称. */
      30000,       /* 任务的堆栈大小 */
      NULL,        /* 任务的参数 */
      1,           /* 任务的优先级 */
      &xTask1,     /* 跟踪创建的任务的任务句柄 */
      0);          /* pin任务到核心0 */

  xTaskCreatePinnedToCore(
      Core1Task,
      "Core1Task", /* 任务名称. */
      30000,       /* 任务的堆栈大小 */
      NULL,        /* 任务的参数 */
      1,           /* 任务的优先级 */
      &xTask2,     /* 跟踪创建的任务的任务句柄 */
      1);          /* pin任务到核心0 */
}

void loop()
{

  // // Serial.printf("Main Loop is OK");
  // // MCAL_Gpio_Write(PIN_ledblue,1);

  // //SWC Layer
  // SWC_PowerManger();
  // SWC_Map();
  // SWC_ReplayMap();
  // SWC_BLE_Act();
  // SWC_BLE_Updata();
  // // Serial.printf(" \r\n");
  // // MCAL_Gpio_Write(PIN_ledblue,0);
}

void Core0Task(void *parameter)
{
  while (1)
  {
    delay(5);
    // delayMicroseconds(1000);
    SWC_PowerManger();
    SWC_Map();
    SWC_BLE_Act();

    if (Flag_500ms)
    {
      Flag_500ms = 0;
      SWC_ScreenShowBatter();

      SWC_BLE_Updata();
    }
    
  }
  vTaskDelete(NULL);
}
void Core1Task(void *parameter)
{
  while (1)
  {
    // delay(1);
    // delayMicroseconds(1000);

    SWC_ReplayMap();
    RTE_I2C(); /** RTE层 I2C总线执行模块*/
   
  }
  vTaskDelete(NULL);
}
