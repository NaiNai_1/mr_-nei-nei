
#include "R_config.h"



                          
R_Queue_t I2C_PopList[MAX_LEN_OF_QUEUE];           /// @brief I2C总线输出队列
R_Queue_t I2C_PushList[MAX_LEN_OF_QUEUE];          /// @brief I2C总线输入队

void setup() {

  Serial.begin(115200);
  MCAL_Init();
  MCAL_PowerKeep();
  MCAL_FrequencyInit();
  MCAL_BLEInit();

  //SWC Value Init
  swc_map_sta = CREATE_MAP;
}

void loop() {

  // Serial.printf("Main Loop is OK");
  // MCAL_Gpio_Write(PIN_ledblue,1);
  

  //SWC Layer
  SWC_PowerManger();
  SWC_Map(&swc_map_sta);
  SWC_ReplayMap(&swc_map_sta);
  SWC_BLE_Act();
  // Serial.printf(" \r\n");
  // MCAL_Gpio_Write(PIN_ledblue,0);
  
}








