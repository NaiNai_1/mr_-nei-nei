#include "R_config.h"

//调用的类声名


//// SWC层

///@brief 电源管理SWC
void SWC_PowerManger()
{
//   uint16_t reg_data = 0;  
//   reg_data = MCAL_Gpio_AnalogRead(PIN_powerkey);
//   Serial.println(reg_data);  

  if (MCAL_Gpio_AnalogRead(PIN_powerkey) >= 3000)
  {
    MCAL_Gpio_Write(PIN_powerEN, 0);
    /// @brief 松手才开始执行
    while(MCAL_Gpio_AnalogRead(PIN_powerkey) >= 3000);   
    delay(500);
  } 
  else 
  {
    MCAL_Gpio_Write(PIN_powerEN, 1); 
  }
}

///@brief 线路记录SWC

void SWC_Map_RPort_LWheelF(uint8_t LWheel_F)
{
    Rte_Wheel_Front.LWheel = LWheel_F;
}
void SWC_Map_RPort_RWheelF(uint8_t RWheel_F)
{
    Rte_Wheel_Front.RWheel = RWheel_F;
}
WheelPosition_t WheelPosition_Map[MaxMapSize];
static uint16_t Map_Index = 0;
uint16_t SWC_Map_ReadMapIndex()
{
    return Map_Index;
}
void SWC_Map_ResetMapIndex()
{
    Map_Index = 0 ;
}
void SWC_Map(SWC_MAP_STA_t *p_swc_map_sta)
{
    /** 
     * position
     * ↑
     * |
     * |
     * |
     * 丄---------->time
    */
    
    
    static SWC_MAP_STA_t swc_map_sta_ex = CREATE_MAP;
    static WheelPosition_t WheelPosition;
    static WheelPosition_t WheelPosition_ex;
    static uint8_t Wheel_Front;
    if ((*p_swc_map_sta) == CREATE_MAP && swc_map_sta_ex == REPLAY_MAP)
    {
        SWC_Map_ResetMapIndex();
    }

    swc_map_sta_ex = (*p_swc_map_sta);
    if((*p_swc_map_sta) == REPLAY_MAP)
    {

        return;
    }
    
    
    
    /***********************************************************************/
    /*                               地图绘制                               */
    /***********************************************************************/
    //写爆数组移位处理    
    if (Map_Index == MaxMapSize)
    {
        Map_Index--;
        int i;
        for (i = 0; i < MaxMapSize - 1; i++) {
            WheelPosition_Map[i].psLeft =  WheelPosition_Map[i + 1].psLeft;
            WheelPosition_Map[i].psRight =  WheelPosition_Map[i + 1].psRight;
        }
        
    }

    //坐标系起点设定
    if (Map_Index == 0)
    {
        BSW_WheelRotation(&WheelPosition_Map[0].psLeft,&WheelPosition_Map[0].psRight);
    }
    else if (Map_Index < MaxMapSize) 
    {
        WheelPosition_ex = WheelPosition;               /** 车轮输出值坐标系 深度 */
        BSW_WheelRotation(&WheelPosition.psLeft,&WheelPosition.psRight);
    
        if (abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight) < 2 &&
        abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft) < 2)
        {
            return;
        }

        //获取车轮位置 + 判断方向
       
        if (Rte_Wheel_Front.LWheel == W_F)              /** 左轮：车轮向前*/
        {

            WheelPosition_Map[Map_Index].psLeft     = WheelPosition_Map[Map_Index - 1].psLeft + (abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;            
        }
        else if (Rte_Wheel_Front.LWheel == W_R)
        {
            WheelPosition_Map[Map_Index].psLeft     = WheelPosition_Map[Map_Index - 1].psLeft - (abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;
        }
        else
        {
            WheelPosition_Map[Map_Index].psLeft = WheelPosition_Map[Map_Index - 1].psLeft;
        }

        if (Rte_Wheel_Front.RWheel == W_F)              /** 左轮：车轮向后*/
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight + (abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
        }
        else if (Rte_Wheel_Front.RWheel == W_R)         /** 右轮：车轮向后*/
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight - (abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
        }
        else
        {
            WheelPosition_Map[Map_Index].psRight    = WheelPosition_Map[Map_Index - 1].psRight;
        }
        
        if (Map_Index == MaxMapSize -1)
        {
            MCAL_Gpio_Write(PIN_ledblue,1);
        }
        else
        {
            MCAL_Gpio_Write(PIN_ledblue,0);
        }
        
        Serial.print(Map_Index);
        Serial.print(",");
        Serial.print(WheelPosition_Map[Map_Index].psLeft);
        Serial.print(",");
        Serial.println(WheelPosition_Map[Map_Index].psRight);

        // Serial.print(abs(WheelPosition.psRight));
        // Serial.print("-");
        // Serial.print(abs(WheelPosition_ex.psRight));
        // Serial.print("=");
        // Serial.println((abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)));
    }   
    Map_Index++;

    /** 打印整个表 */
    // Serial.printf("Map:");
    // for(int table = 0;table < MaxMapSize; table++)
    // {
    //     Serial.printf("{%d,%d},",WheelPosition_Map[table].psLeft,WheelPosition_Map[table].psRight);
    //     if (table%10 == 0)
    //     {
    //         Serial.printf("\r\n");
    //     }
    // }
    // Serial.printf("\r\n");
}
bool StartReplay = 0;
void SWC_ReplayMap(SWC_MAP_STA_t *p_swc_map_sta)
{
    static WheelPosition_t WheelPosition_Point;
    static uint16_t PointIndex = 0;                          /** 当前执行的标点*/
    // static uint8_t PointIndex_R = 0;                          /** 当前执行的标点*/
    static SWC_MAP_STA_t swc_map_sta_ex;
    uint8_t WheelFront = 0;
    float Pid_out_L;
    float Pid_out_R;
    /** 物理量坐标系*/
    static WheelPosition_t WheelPosition;
    static WheelPosition_t WheelPosition_ex;
    /** 重播坐标系*/
    static WheelPosition_t Wheel_ReplayPoint;              /** 车轮物理位置_当前值 */ 
    static uint8_t ReplaySta = 0;


    /** SWC 测试 模拟 Map*/
    // WheelPosition_t reg_map[] = 
    // {
    //    {-1100, 1100},{-990, 990},{-980, 980},{-970, 970},{-960, 960},{-950, 950},{-940, 940},{-930, 930},{-920, 920},{-910, 910},
    //    {-900, 900},{-890, 890},{-880, 880},{-870, 870},{-860, 860},{-850, 850},{-840, 840},{-830, 830},{-820, 820},{-810, 810},
    //    {-800, 800},{-790, 790},{-780, 780},{-770, 770},{-760, 760},{-750, 750},{-740, 740},{-730, 730},{-720, 720},{-710, 710},
    //    {-700, 700},{-690, 690},{-680, 680},{-670, 670},{-660, 660},{-650, 650},{-640, 640},{-630, 630},{-620, 620},{-610, 610},
    //    {-600, 600},{-590, 590},{-580, 580},{-570, 570},{-560, 560},{-550, 550},{-540, 540},{-530, 530},{-520, 520},{-510, 510},
    //    {-500, 500},{-490, 490},{-480, 480},{-470, 470},{-460, 460},{-450, 450},{-440, 440},{-430, 430},{-420, 420},{-410, 410},
    //    {-400, 400},{-390, 390},{-380, 380},{-370, 370},{-360, 360},{-350, 350},{-340, 340},{-330, 330},{-320, 320},{-310, 310},
    //    {-300, 300},{-290, 290},{-280, 280},{-270, 270},{-260, 260},{-250, 250},{-240, 240},{-230, 230},{-220, 220},{-210, 210},
    //    {-200, 200},{-190, 190},{-180, 180},{-170, 170},{-160, 160},{-150, 150},{-140, 140},{-130, 130},{-120, 120},{-110, 110},
    //    {-100, 100},{-90, 90},{-80, 80},{-70, 70},{-60, 60},{-50, 50},{-40, 40},{-30, 30},{-20, 20},{-10, 10},{0, 0}
    // };
    // memcpy(WheelPosition_Map,reg_map,sizeof(WheelPosition_Map));

    
    if (((*p_swc_map_sta) != swc_map_sta_ex && (*p_swc_map_sta) == REPLAY_MAP) || StartReplay)
    {
        /**开始回放*/
        StartReplay = 0;
        swc_map_sta_ex = (*p_swc_map_sta);
        // PointIndex = 0;
         PointIndex = SWC_Map_ReadMapIndex() - 1;
        Serial.printf("/****************************************************************/\r\n");
        Serial.printf("/*                       Start replay map                       */\r\n");
        Serial.printf("/****************************************************************/\r\n");
    }
    
    if ((*p_swc_map_sta) == REPLAY_MAP)
    {
        // Serial.printf("Point:%d ",PointIndex);
        // Serial.printf("Map:%d ",SWC_Map_ReadMapIndex());
        
        // Serial.printf("Map.Left:");
        // Serial.printf("%d ",WheelPosition_Map[PointIndex].psLeft);
        // Serial.printf("Map.Right:");
        // Serial.printf("%d ",WheelPosition_Map[PointIndex].psRight);
        // Serial.printf("ReplayPoint.Left:");
        // Serial.printf("%d ",Wheel_ReplayPoint.psLeft);
        // Serial.printf("ReplayPoint.Right:");
        // Serial.printf("%d ",Wheel_ReplayPoint.psRight);
        // Serial.printf("\r\n");
        if (PointIndex == SWC_Map_ReadMapIndex() - 1/* PointIndex == 0 */)
        {
            /** 设置重播点的位置*/
            Wheel_ReplayPoint = WheelPosition_Map[PointIndex];
        }
        else
        {
             /** 更新重播点的位置*/
            WheelPosition_ex = WheelPosition;
            BSW_WheelRotation(&WheelPosition.psLeft,&WheelPosition.psRight);
            if (Rte_Wheel_Front.LWheel == W_F)    
            {

                Wheel_ReplayPoint.psLeft     = Wheel_ReplayPoint.psLeft + (abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;            
            }
            else if (Rte_Wheel_Front.LWheel == W_R)
            {
                Wheel_ReplayPoint.psLeft     = Wheel_ReplayPoint.psLeft - (abs(WheelPosition.psLeft) - abs(WheelPosition_ex.psLeft)) ;
            }
            else
            {
                Wheel_ReplayPoint.psLeft = Wheel_ReplayPoint.psLeft;
            }

            if (Rte_Wheel_Front.RWheel == W_F)
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight + (abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
            }
            else if (Rte_Wheel_Front.RWheel == W_R)
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight - (abs(WheelPosition.psRight) - abs(WheelPosition_ex.psRight)) ;
            }
            else
            {
                Wheel_ReplayPoint.psRight    = Wheel_ReplayPoint.psRight;
            }
        }
       
        
        /** PID */
        Pid_out_L = SWC_ReplayMap_PIDControl_L(WheelPosition_Map[PointIndex].psLeft,Wheel_ReplayPoint.psLeft);
        Pid_out_R = SWC_ReplayMap_PIDControl_R(WheelPosition_Map[PointIndex].psRight,Wheel_ReplayPoint.psRight);
        Pid_out_L = Pid_out_L > 200 ? 200 : Pid_out_L;
        Pid_out_L = Pid_out_L < -200 ? -200 : Pid_out_L;
        Pid_out_R = Pid_out_R > 200 ? 200 : Pid_out_R;
        Pid_out_R = Pid_out_R < -200 ? -200 : Pid_out_R;
        // Serial.printf("PID_L:%f,PID_R:%f ",Pid_out_L,Pid_out_R);
        // Serial.printf("\r\n");
        BSW_WheelDriver(MOTO_L,POWER_DRIVE,Pid_out_L);
        BSW_WheelDriver(MOTO_R,POWER_DRIVE,Pid_out_R);

        /** 重播点控制 */

        if (
            (WheelPosition_Map[PointIndex].psLeft - 50 < Wheel_ReplayPoint.psLeft && Wheel_ReplayPoint.psLeft < WheelPosition_Map[PointIndex].psLeft + 50)
            &&
            (WheelPosition_Map[PointIndex].psRight - 50 < Wheel_ReplayPoint.psRight && Wheel_ReplayPoint.psRight < WheelPosition_Map[PointIndex].psRight + 50)
            )
        {
            // PointIndex++;
            // if (PointIndex == SWC_Map_ReadMapIndex())
            // { 
            //     PointIndex = SWC_Map_ReadMapIndex() - 1;
            // }
            
            if (PointIndex == 0)
            {
                PointIndex = 1;
            }
            PointIndex--;
        }

        // if (PointIndex_R == MaxMapSize - 1 && PointIndex_L == MaxMapSize - 1)
        // {
        //     /** 回放结束 */
        //     (*p_swc_map_sta) = CREATE_MAP;
        //     swc_map_sta_ex = CREATE_MAP;
        // }

    }
    
    
}
void SWC_ReplayMap_Start()
{
    StartReplay = 1;
}
float SWC_ReplayMap_PIDControl_LV2_L(int16_t target,int16_t current)
{
    static float error = 0;
    static float integral = 0;
    static float derivative = 0;
    static float lastError = 0;
    int16_t _float = 30; /** 进入第二层PID */
}
float SWC_ReplayMap_PIDControl_L(int16_t target,int16_t current)
{
    // PID控制器变量
    static float error = 0;
    static float integral = 0;
    static float derivative = 0;
    static float lastError = 0;
    static float Output;
    int16_t _float = 30; /** 进入第二层PID */
    //数值接近则清除积分值
    if (target - _float < current && current < target + _float)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    if (abs(target - current) < 20 && Output > 150)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    
    error = target - current;
    integral += error;
    derivative = error - lastError;
    lastError = error;
    // Serial.printf("PID:%f,%f,%f",Kp * error,Ki * integral,Kd * derivative);
    // Serial.printf("\r\n");
    Output = (Kp * error) + (Ki * integral) + (Kd * derivative);
    return Output;
}
float SWC_ReplayMap_PIDControl_R(int16_t target,int16_t current)
{
    // PID控制器变量
    static float error = 0;
    static float integral = 0;
    static float derivative = 0;
    static float lastError = 0;
    static float Output;
    int16_t _float = 30; /** 进入第二层PID */
    //数值接近则清除积分值
    if (target - _float < current && current < target + _float)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    if (abs(target - current) < 20 && Output > 150)
    {
        error = 0;
        integral = 0;
        derivative = 0;
        lastError = 0;
        return 0;
    }
    
    error = target - current;
    integral += error;
    derivative = error - lastError;
    lastError = error;
    // Serial.printf("PID:%f,%f,%f",Kp * error,Ki * integral,Kd * derivative);
    // Serial.printf("\r\n");
    Output = (Kp * error) + (Ki * integral) + (Kd * derivative);
    return Output;
}
///@brief 遥控信号执行SWC BLE信号分发到 MOTO/LED
void SWC_BLE_Act()
{
    static uint8_t reg_data[20];
    static uint8_t ble_data_ex[20];
    uint16_t datalen;
    int BleToWheel_R;
    int BleToWheel_L;


    /// @brief 蓝牙消息管理部分
    memcpy(ble_data_ex,reg_data,sizeof(reg_data));
    if (Queue_Pop(BLE_RecvQueue,reg_data,&datalen) == true)
    {

        //指示灯工作
        if(swc_map_sta == REPLAY_MAP)
        {
            MCAL_Gpio_Write(PIN_ledred,0);
        } 
        else
        {
            MCAL_Gpio_Write(PIN_ledred,1);
        }

        //驱动电机
        if (swc_map_sta == CREATE_MAP)
        {
            BleToWheel_L = (int8_t)reg_data[5] * 40;
            BleToWheel_R = (int8_t)reg_data[6] * 40;
            BSW_WheelDriver(MOTO_L,POWER_DRIVE,BleToWheel_L);
            BSW_WheelDriver(MOTO_R,POWER_DRIVE,BleToWheel_R);
        }
        
        

        //是否允许地图回放功能
        if(memcmp(reg_data,ble_data_ex,datalen)  != 0)
        {
            if(reg_data[8]%2 == 1)
            {
                swc_map_sta = REPLAY_MAP;
                SWC_ReplayMap_Start();
            }
            else
            {
                swc_map_sta = CREATE_MAP;
            }
        }
        
        

        //打印队列
      
        // Serial.printf("Queue =>");
        // for (int i = 0; i < datalen; i++)
        // {
        //   Serial.printf("%x ",reg_data[i]);
        // }
        // Serial.printf("    ");
        // Serial.printf("Wheel Data:");
        // Serial.printf("%d && %d",BleToWheel_L,BleToWheel_R);
        // Serial.printf("    ");
        // Serial.printf("swc_map_sta:");
        // Serial.printf(swc_map_sta == REPLAY_MAP?"REPLAY_MAP":"CREATE_MAP");
        // Serial.printf("\r\n");

        
    }
    /// @brief 蓝牙功能管理部分
    MCAL_BLEMsgManager();
    
}


