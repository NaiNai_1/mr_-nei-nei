#include "R_config.h"
#include "Wire.h"
#include <ESP32Servo.h>
#include <Arduino.h>
//调用的类声明
#define ENABLE_Wheel_Front_Print 0

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;

Servo servo1;
Servo servo2;

hw_timer_t * timer = NULL;                        /// @brief 声明一个定时器

/// @brief 蓝牙Callback
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      uint8_t reg_data[20];
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        // Serial.printf("LE =>");
        for (int i = 0; i < rxValue.length(); i++)
        {
        //   Serial.printf("%x ",rxValue[i]);
          reg_data[i] = rxValue[i];
        }
        // Serial.printf("\r\n");
        Queue_Push(BLE_RecvQueue,reg_data,(uint16_t)rxValue.length());
      }
    }
};

///@brief 蓝牙处理基础软件
void MCAL_BLEInit()
{
    // Create the BLE Device
  BLEDevice::init("TUDAO_ESP32");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
										CHARACTERISTIC_UUID_TX,
										BLECharacteristic::PROPERTY_NOTIFY
									);
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
											 CHARACTERISTIC_UUID_RX,
											BLECharacteristic::PROPERTY_WRITE
										);

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}
void MCAL_BLEMsgManager()
{
    int8_t reg_data = 0;
    // connected
    if (deviceConnected)
    {

    }
    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
		// do stuff here on connecting
        oldDeviceConnected = deviceConnected;
        // delay(500);
        // MCAL_BLESend((uint8_t *)reg_data,1);
    }
}
void MCAL_BLESend(uint8_t *pData,uint32_t datalen)
{
    pTxCharacteristic->setValue(pData, datalen);
    pTxCharacteristic->notify();
}
void MCAL_BLEAdvertising()
{
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
}
void MCAL_BLEConneting()
{

}
///@brief 电机PWM驱动基础软件
void MCAL_FrequencyInit()
{
    servo1.setPeriodHertz(500);      // Standard 50hz servo
    servo2.setPeriodHertz(500);      // Standard 50hz servo
    servo1.attach(PIN_M_L);//13
    servo2.attach(PIN_M_R);//14
    servo1.writeMicroseconds(1500);
    servo2.writeMicroseconds(1500);
}
void MCAL_FrequencyPush(uint8_t channel,uint16_t Frequency)
{
    switch (channel)
    {
    case 0:
        //发送转速
        servo1.writeMicroseconds(Frequency);
        if (Frequency < 1500 - 15)
        {
            SWC_Map_RPort_LWheelF(0);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel Front");
            #endif
        }
        else if(Frequency > 1500 + 15)
        {
            SWC_Map_RPort_LWheelF(1);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel Back");
            #endif
        }
        else
        {
            SWC_Map_RPort_LWheelF(2);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Left Wheel stop");
            #endif
        }

            #if ENABLE_Wheel_Front_Print
            Serial.printf("\r\n");
            #endif
        break;
    case 1:
        servo2.writeMicroseconds(Frequency);
        if (Frequency > 1500 + 15)
        {
            SWC_Map_RPort_RWheelF(0);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel Front");
            #endif
        }
        else if(Frequency < 1500 - 15)
        {
            SWC_Map_RPort_RWheelF(1);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel Back");
            #endif
        }
        else
        {
            SWC_Map_RPort_RWheelF(2);
            #if ENABLE_Wheel_Front_Print
            Serial.printf("Right Wheel stop");
            #endif
        }

            #if ENABLE_Wheel_Front_Print
            Serial.printf("\r\n");
            #endif
        break;
    default:
        break;
    }
}
///@brief I2C驱动基础软件
void MCAL_I2cWriteStart(uint8_t addr,uint8_t *p_data,uint8_t len,bool StopFlag)
{
    Wire.beginTransmission(addr);
    Wire.write(p_data,len);
    Wire.endTransmission(StopFlag);
}
void MCAL_I2cReadStart(uint8_t addr,uint8_t *p_data,uint32_t len,bool StopFlag)
{
    Wire.requestFrom(addr, len);
    Wire.readBytes(p_data, len);
    Wire.endTransmission(StopFlag);
}


///@brief 定时器初始化
// void MCAL_Timer_Init(hw_timer_t p_timer,uint32_t delay_data)
// {
//     p_timer = timerBegin(0, 80, true);                // 初始化定时器指针        
//     timerAttachInterrupt(timer, &onTimer, true);    // 绑定定时器
//     timerAlarmWrite(timer, delay_data, true);          // 配置报警计数器保护值（就是设置时间）0.1ms
//     timerAlarmEnable(timer);                        // 启用定时器
// }

///@brief MCU引脚驱动
void MCAL_Gpio_Write(uint8_t gpio_number,uint8_t value)
{
    if (value)
    {
        digitalWrite(gpio_number,HIGH);
    }
    else
    {
        digitalWrite(gpio_number,LOW);
    }
}
uint8_t MCAL_Gpio_Read(uint8_t gpio_number)
{
    return digitalRead(gpio_number);
}
uint16_t MCAL_Gpio_AnalogRead(uint8_t gpio_number)
{
    return analogRead(gpio_number);
}
void MCAL_PowerEnable(bool EnableFlag)
{
    if (EnableFlag)
    {
        /* code */
    }
}

void MCAL_Init()
{
    Wire.setPins(23,18);
    Wire.begin();

    pinMode(PIN_powerkey, INPUT);
    pinMode(PIN_key1, INPUT);
    pinMode(PIN_key2, INPUT);
    pinMode(PIN_powerEN, OUTPUT);

    pinMode(PIN_ledred, OUTPUT);
    pinMode(PIN_ledblue, OUTPUT);
    digitalWrite(PIN_powerEN, HIGH);
    // ledcAttachPin(PIN_ledred,1);
    // ledcAttachPin(PIN_ledblue,2);
  
    // ledcSetup(CH_RED, 12000, 8); // 500hz freq, 8 bit resolution, ie. 1ms on, 1ms off per
    // ledcSetup(CH_BLUE, 12000, 8); 
}
void MCAL_PowerKeep()
{
    int i;
  /**
   * 电源键启动
  */
  if (analogRead(PIN_powerkey) >= 3000)
  {
    digitalWrite(PIN_powerEN, HIGH);
    /// @brief 松手才开始执行
    while(analogRead(PIN_powerkey) >= 3000);   
  } 
  else 
  {
    digitalWrite(PIN_powerEN, LOW); 
  }
}