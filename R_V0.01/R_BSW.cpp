#include "R_config.h"

//解码部分
const char CRC8Table[] = { 0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32,
		163, 253, 31, 65, 157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189,
		62, 96, 130, 220, 35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3,
		128, 222, 60, 98, 190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158,
		29, 67, 161, 255, 70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56,
		102, 229, 187, 89, 7, 219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165,
		251, 120, 38, 196, 154, 101, 59, 217, 135, 4, 90, 184, 230, 167, 249,
		27, 69, 198, 152, 122, 36, 248, 166, 68, 26, 153, 199, 37, 123, 58, 100,
		134, 216, 91, 5, 231, 185, 140, 210, 48, 110, 237, 179, 81, 15, 78, 16,
		242, 172, 47, 113, 147, 205, 17, 79, 173, 243, 112, 46, 204, 146, 211,
		141, 111, 49, 178, 236, 14, 80, 175, 241, 19, 77, 206, 144, 114, 44,
		109, 51, 209, 143, 12, 82, 176, 238, 50, 108, 142, 208, 83, 13, 239,
		177, 240, 174, 76, 18, 145, 207, 45, 115, 202, 148, 118, 40, 171, 245,
		23, 73, 8, 86, 180, 234, 105, 55, 213, 139, 87, 9, 235, 181, 54, 104,
		138, 212, 149, 203, 41, 119, 244, 170, 72, 22, 233, 183, 85, 11, 136,
		214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168, 116, 42, 200, 150,
		21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53 };

unsigned char CRC8_Table(unsigned char *p, char counter) {
	unsigned char crc8 = 0;

	for (; counter > 0; counter--) {
		crc8 = CRC8Table[crc8 ^ *p];
		p++;
	}
	return (crc8);

}

//// BSW层 

  //服务器层
void BSW_WheelDriver(MOTO_NUMBER moto,MOTO_FUNCTION fun,int data)//精度250
{
    uint8_t reg_data[20];
    static uint8_t m1_status_ex;
    static uint8_t m2_status_ex;
    static uint8_t m1_status;
    static uint8_t m2_status;
    static uint16_t m1_degree;
    static uint16_t m2_degree;

    m1_status_ex = m1_status;
    m2_status_ex = m2_status;
    switch (moto)
    {
    case MOTO_L:
        m1_status = fun;
        switch (fun)
        {
        case MOTO_BREAK:
            
            break;
        case POWER_DRIVE:
            MCAL_FrequencyPush(0,1500 - data);
            break;
        case SPEED_DRIVE:
            
            break;
        default:
            break;
        }
        break;
    case MOTO_R:
        m2_status = fun;
        switch (fun)
        {
        case MOTO_BREAK:
            
            break;
        case POWER_DRIVE:
            MCAL_FrequencyPush(1,1500 + data);
            break;
        case SPEED_DRIVE:
            
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
    if (
        ((m1_status == 0x02 || m1_status == 0x01) && m1_status_ex == m1_status)
        &&
        ((m2_status == 0x02 || m2_status == 0x01) && m2_status_ex == m2_status))
    {
        return ;    /** 状态未改变时不使用i2c*/
    }
    
    reg_data[0] = 0xA0;
    reg_data[1] = 0x00;
    reg_data[2] = (m1_status << 4) | m2_status;
    reg_data[3] = (m1_degree & 0x00ff);
    reg_data[4] = m1_degree >> 8;
    reg_data[5] = (m2_degree & 0x00ff);
    reg_data[6] = m2_degree >> 8;
    reg_data[7] = CRC8_Table(reg_data,7);
    MCU_I2C_MasterWriteSalve(0x02,reg_data,8);

}

void BSW_WheelRotation(int16_t *Wheel_L,int16_t *Wheel_R)
{
    uint8_t Reg_data[11];
    MCU_I2C_MasterReadSalve(MOTODTIVER_ADDR,Reg_data,sizeof(Reg_data));
    *Wheel_L      = (int16_t)(Reg_data[7]<<8 | Reg_data[6]);
    *Wheel_R      = (int16_t)(Reg_data[9]<<8 | Reg_data[8]);

    // Serial.print(*Wheel_L);
    // Serial.print(",");
    // Serial.println(*Wheel_R);

}

// void BSW_RGB(RGB_NUMBER number,RGB_Color color)
// {
//     switch (number)
//     {
//     case 0:
        
//         break;
//     case 1:
        
//         break;
//     default:
//         break;
//     }
// }
void BSW_LED(LED_NUMBER number,LED_FUNCTION fun )
{
    switch (fun)
    {
    case Breat:
        /* code */
        break;
    case _500msFlash:
        /* code */
        break;
    case _1000msFlash:
        /* code */
        break;
    default:
        break;
    }
}
void BSW_GetBleWheelOder(uint8_t)
{
    
}
  //MCU抽象层
void MCU_I2C_MasterWriteSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len)
{
    uint8_t Reg_CMD = 0x01;
    MCAL_I2cWriteStart(addr,p_data,data_len,false);
    MCAL_I2cReadStart(addr,&Reg_CMD,1,true);
    Serial.printf("writer data %d : ",addr);
    for(int i = 0; i<data_len;i++ )
    {
         Serial.printf("%x ",p_data[i]);
    }
    Serial.print("\r\n");
}
void MCU_I2C_MasterReadSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len)
{
    uint8_t Reg_CMD = 0x10;
    uint8_t i = 0;
    MCAL_I2cWriteStart(addr,&Reg_CMD,1,false);
    MCAL_I2cReadStart(addr,p_data,data_len,true);

    // Serial.printf("read data %d : ",addr);
    // for(i = 0; i<data_len;i++ )
    // {
    //      Serial.printf("%x ",p_data[i]);
    // }
    // Serial.print("\r\n");
}

  //复杂驱动
  //MCAL层 - 目前在R_MCAL.cpp中