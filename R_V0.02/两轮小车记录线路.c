#include <stdio.h>

// 定义车的初始位置
int positionX = 0;
int positionY = 0;

// 定义车的初始方向
int direction = 0;  // 0代表北，1代表东，2代表南，3代表西

// 定义车轮走过的轨迹
typedef struct {
    int x;
    int y;
} Trail;

Trail trail[100];  // 用数组保存车轮走过的轨迹
int trailCount = 0;  // 记录车轮走过的轨迹数量

// 前进函数
void moveForward(int steps) {
    switch (direction) {
        case 0:  // 北
            positionY += steps;
            break;
        case 1:  // 东
            positionX += steps;
            break;
        case 2:  // 南
            positionY -= steps;
            break;
        case 3:  // 西
            positionX -= steps;
            break;
    }
    
    // 记录车轮走过的轨迹
    trail[trailCount].x = positionX;
    trail[trailCount].y = positionY;
    trailCount++;
}

// 后退函数
void moveBackward(int steps) {
    switch (direction) {
        case 0:  // 北
            positionY -= steps;
            break;
        case 1:  // 东
            positionX -= steps;
            break;
        case 2:  // 南
            positionY += steps;
            break;
        case 3:  // 西
            positionX += steps;
            break;
    }
    
    // 记录车轮走过的轨迹
    trail[trailCount].x = positionX;
    trail[trailCount].y = positionY;
    trailCount++;
}

// 转向函数
void turn(int clockwise) {
    if (clockwise) {
        direction = (direction + 1) % 4;
    } else {
        direction = (direction + 3) % 4;
    }
}

// 打印车的当前状态
void printCarStatus() {
    printf("当前位置：(%d, %d)\n", positionX, positionY);
    printf("当前方向：");
    switch (direction) {
        case 0:
            printf("北\n");
            break;
        case 1:
            printf("东\n");
            break;
        case 2:
            printf("南\n");
            break;
        case 3:
            printf("西\n");
            break;
    }
}

int main() {
    moveForward(3);  // 前进3步
    turn(1);  // 向右转
    moveForward(2);  // 前进2步
    turn(0);  // 向左转
    moveBackward(1);  // 后退1步
    
    printCarStatus();  // 打印车的当前状态
    
    // 打印车轮走过的轨迹
    printf("车轮走过的轨迹：\n");
    for (int i = 0; i < trailCount; i++) {
        printf("(%d, %d) ", trail[i].x, trail[i].y);
    }
    printf("\n");
    
    return 0;
}

/**********************************************************/
/*               驱动直流电机转到某一角度的PID              */
/**********************************************************/
#include <stdio.h>

// PID参数
#define Kp 1.0
#define Ki 0.5
#define Kd 0.2

// 目标角度
#define targetAngle 90

// 当前角度
float currentAngle = 0;

// PID控制器变量
float error = 0;
float integral = 0;
float derivative = 0;
float lastError = 0;

// 更新当前角度
void updateCurrentAngle(float angle) {
    currentAngle = angle;
}

// PID控制器
float pidController() {
    error = targetAngle - currentAngle;
    integral += error;
    derivative = error - lastError;
    lastError = error;

    return (Kp * error) + (Ki * integral) + (Kd * derivative);
}

int main() {
    // 模拟电机转动
    for (int i = 0; i < 100; i++) {
        // 假设每次循环更新当前角度
        updateCurrentAngle(i);

        // 使用PID控制器计算输出
        float output = pidController();

        // 输出控制信号，驱动电机转动
        printf("Control signal: %.2f\n", output);
    }

    return 0;
}
