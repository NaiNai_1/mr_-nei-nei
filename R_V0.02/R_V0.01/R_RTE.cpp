#include "R_config.h"

///@brief RTE层


//电机方向
RTE_WHEEL_FRONT_t Rte_Wheel_Front;

//SWC的路线回放功能执行状态
SWC_MAP_STA_t swc_map_sta;


//地图队列
R_Queue_t Map_List[2];

/**RTOS队列*/
xQueueHandle xQueue_Map;
xQueueHandle xQueue_WheelRotation;  /** 车轮位置队列*/
xQueueHandle xQueue_WheelDriver;    
xQueueHandle xQueue_ScreenShowPixcel;
xQueueHandle xQueue_BLE_Recv;       /** BLE消息队列*/




void RTE_I2C(void)
{
    
    WheelPosition_t WheelRotationMsg;           /** 车轮输出*/
    uint32_t    ScreenShowPixcelMsg;            /** 屏幕输入*/
    WheelDriverMsg_t WheelDriverMsg;            /** 车轮输入*/

    #if EN_TIME_LOG
    Serial.printf("RTE_I2C() -> millis before iic:%d \r\n",millis());
    #endif

    if(swc_map_sta != CREATE_MAP)
    {
        return;
    }
    /** 车轮*/
    BSW_WheelRotation(&WheelRotationMsg.psLeft,&WheelRotationMsg.psRight);              /** I2C获取车轮位置*/
    if (xQueueSendToFront( xQueue_WheelRotation, &WheelRotationMsg, 0/*pdMS_TO_TICKS(100)*/ ) != pdPASS)              
    {
        xQueueReset(xQueue_WheelRotation);                                              /** 重置队列*/   
        #if EN_I2C_LOG
        Serial.printf("I2C Queue Push -> Error\r\n");
        #endif
    }
    #if EN_I2C_LOG   
    else
    {
        Serial.printf("I2C Queue Push -> WheelRotationMsg.psLeft: %d, WheelRotationMsg.psRight: %d, Time Count:%d\r\n",WheelRotationMsg.psLeft,WheelRotationMsg.psRight,Clock_Counter);
    }
    #endif

    #if EN_TIME_LOG
    Serial.printf("     RTE_I2C() -> millis after  iic:%d \r\n",millis());
    #endif
    
    
    while ( xQueueReceive( xQueue_WheelDriver, &WheelDriverMsg, 0 ) == pdPASS)
    {
        #if EN_WHEEL_LOG||EN_RTE_LOG
        Serial.printf("Whell Pop , Time Count: %d\r\n",Clock_Counter);
        #endif
        BSW_WheelDriver(WheelDriverMsg.moto,WheelDriverMsg.fun,WheelDriverMsg.data);
    }
    
    

    /** 屏幕*/
    if (xQueueReceive( xQueue_ScreenShowPixcel, &ScreenShowPixcelMsg, 0 ) == pdPASS)
    {
        #if  EN_SCREEN_POP_LOG
        Serial.printf("Screen Pop\r\n");
        #endif
        BSW_ScreenShowPixcel(ScreenShowPixcelMsg);
    }
    
}