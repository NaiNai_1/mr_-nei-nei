
#ifndef R_BSW_H
#define R_BSW_H

#include "R_config.h"

#define BSW_MOTOR_CTRL_CTL_TYPE_STOP							(0)
#define BSW_MOTOR_CTRL_CTL_TYPE_BRAKE							(1)
#define BSW_MOTOR_CTRL_CTL_TYPE_PWR								(2)
#define BSW_MOTOR_CTRL_CTL_TYPE_SPEED							(3)

/// @brief led灯板
#define LED_5X5_SENSITIVE_MATRIX_IIC_ADDR                         (0x03)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_READ                     (0x10)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_READ_LEN                 (1 + 2)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SHOW_IMG                 (0xA0)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SHOW_IMG_LEN             (1 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_PIXCEL               (0xA1)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_PIXCEL_LEN           (4 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_START          (0xA2)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_START_LEN (1 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_CONTENT (0xA3)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_CONTENT_LEN (12 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_END (0xA4)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_WRITE_STR_END_LEN (1 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_BRINGHTNESS          (0xA5)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_BRINGHTNESS_LEN      (2 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_CLEAR_OFF (0xA6)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_CLEAR_OFF_LEN (1 + 3)

#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_DIR (0xA7)
#define LED_5X5_SENSITIVE_MATRIX_COMMAND_SET_DIR_LEN (1 + 3)

typedef enum
{
  MOTO_L,
  MOTO_R
}MOTO_NUMBER;

typedef enum
{
  MOTO_BREAK = 0x01,
  POWER_DRIVE,
  SPEED_DRIVE
}MOTO_FUNCTION;
typedef enum
{
  MOTO_FF,
  MOTO_FB
}MOTO_SIDE;

//void BSW_RGB(RGB_NUMBER number,LED_Color color)
typedef enum
{
  REB_0,
  REB_1
}REB_NUMBER;
typedef enum
{
  RED,
  GREEN,
  BLUE
}RGB_Color;
//void BSW_LED(LED_NUMBER number,LED_FUNCTION fun )
typedef enum
{
  LED_REG,
  LED_BLUE
}LED_NUMBER;
typedef enum
{
  Breat,
  _500msFlash,
  _1000msFlash
}LED_FUNCTION;


void BSW_Set100usClock();
void BSW_ScreenShowPhoto(uint8_t PhotoNumber);
void BSW_ScreenShowPixcel(uint32_t Pixcel);
void BSW_ScreenShowChar(uint8_t *Char, uint8_t CharNum);

void BSW_WheelDriver(MOTO_NUMBER moto,MOTO_FUNCTION fun,int data);
void BSW_WheelRotation(int16_t *Wheel_L,int16_t *Wheel_R);
// void BSW_RGB(RGB_NUMBER number,RGB_Color color);
void BSW_LED(LED_NUMBER number,LED_FUNCTION fun );

void MCU_I2C_MasterWriteSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len);
void MCU_I2C_MasterReadSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len);
#endif