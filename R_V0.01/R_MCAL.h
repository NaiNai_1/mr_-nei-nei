
#ifndef R_MCAL_H
#define R_MCAL_H

#include "R_config.h"

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"


void MCAL_BLEInit();
void MCAL_BLEMsgManager();
void MCAL_BLESend(uint8_t *pData,uint32_t datalen);

void MCAL_FrequencyInit();
void MCAL_FrequencyPush(uint8_t channel,uint16_t Frequency);
void MCAL_I2cWriteStart(uint8_t addr,uint8_t *p_data,uint8_t len,bool StopFlag);
void MCAL_I2cReadStart(uint8_t addr,uint8_t *p_data,uint32_t len,bool StopFlag);
// void MCAL_Timer_Init(hw_timer_t p_timer,uint32_t delay_data);


void MCAL_Gpio_Write(uint8_t gpio_number,uint8_t value);
uint8_t MCAL_Gpio_Read(uint8_t gpio_number);
uint16_t MCAL_Gpio_AnalogRead(uint8_t gpio_number);
void MCAL_PowerEnable(bool EnableFlag);

void MCAL_Init();
void MCAL_PowerKeep();
#endif