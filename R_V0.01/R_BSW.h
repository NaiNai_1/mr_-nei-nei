
#ifndef R_BSW_H
#define R_BSW_H

#include "R_config.h"

#define BSW_MOTOR_CTRL_CTL_TYPE_STOP							(0)
#define BSW_MOTOR_CTRL_CTL_TYPE_BRAKE							(1)
#define BSW_MOTOR_CTRL_CTL_TYPE_PWR								(2)
#define BSW_MOTOR_CTRL_CTL_TYPE_SPEED							(3)

typedef enum
{
  MOTO_L,
  MOTO_R
}MOTO_NUMBER;

typedef enum
{
  MOTO_BREAK = 0x01,
  POWER_DRIVE,
  SPEED_DRIVE
}MOTO_FUNCTION;
typedef enum
{
  MOTO_FF,
  MOTO_FB
}MOTO_SIDE;

//void BSW_RGB(RGB_NUMBER number,LED_Color color)
typedef enum
{
  REB_0,
  REB_1
}REB_NUMBER;
typedef enum
{
  RED,
  GREEN,
  BLUE
}RGB_Color;
//void BSW_LED(LED_NUMBER number,LED_FUNCTION fun )
typedef enum
{
  LED_REG,
  LED_BLUE
}LED_NUMBER;
typedef enum
{
  Breat,
  _500msFlash,
  _1000msFlash
}LED_FUNCTION;


void BSW_WheelDriver(MOTO_NUMBER moto,MOTO_FUNCTION fun,int data);
void BSW_WheelRotation(int16_t *Wheel_L,int16_t *Wheel_R);
// void BSW_RGB(RGB_NUMBER number,RGB_Color color);
void BSW_LED(LED_NUMBER number,LED_FUNCTION fun );

void MCU_I2C_MasterWriteSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len);
void MCU_I2C_MasterReadSalve(uint8_t addr ,uint8_t *p_data,uint32_t data_len);
#endif